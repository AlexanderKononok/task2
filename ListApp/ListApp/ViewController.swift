//
//  ViewController.swift
//  ListApp
//
//  Created by Alexander Kononok on 9/14/21.
//

import UIKit

class ViewController: UIViewController {
    
    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        configureTableView()
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        setTableViewDelegates()
        tableView.rowHeight = 60
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.pin(to: view)
    }
    
    func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CustomTableViewCell
        let image = UIImage(named:"person")
        cell.set(image: image!,
                 title: "Title \(indexPath.row)",
                 description: "Description \(indexPath.row)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rootVC = SecondViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navigationController?.pushViewController(navVC, animated: true)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
        rootVC.iconImageView.image = UIImage(named: "person")
        rootVC.titleLable.text = "Title \(indexPath.row)"
        rootVC.descriptionLabel.text = "Description \(indexPath.row)"
    }
    
}

extension UIView {
    
    func pin(to superView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.topAnchor).isActive = true
        leadingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.trailingAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
}
