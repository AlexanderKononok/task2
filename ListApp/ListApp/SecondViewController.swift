//
//  SecondViewController.swift
//  ListApp
//
//  Created by Alexander Kononok on 9/20/21.
//

import UIKit

class SecondViewController: UIViewController {

    var iconImageView = UIImageView()
    var titleLable = UILabel()
    var descriptionLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(dismissSelf))
        
        view.addSubview(iconImageView)
        view.addSubview(titleLable)
        view.addSubview(descriptionLabel)
        configureImageView()
        configureTitleLabel()
        configureDescriptionLabel()
        
        setImageConstraints()
        setTitleLabelConstraints()
        setDescriptionLabelConstraints()
    }

    @objc private func dismissSelf() {
        dismiss(animated: true, completion: nil)
    }
    
    func configureImageView() {
        iconImageView.layer.cornerRadius = 50
        iconImageView.backgroundColor = .systemGray
        iconImageView.clipsToBounds = true
    }
    
    func configureTitleLabel() {
        titleLable.adjustsFontSizeToFitWidth = true
    }
    
    func configureDescriptionLabel() {
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.textColor = .systemGray
    }
    
    func setImageConstraints() {
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func setTitleLabelConstraints() {
        titleLable.translatesAutoresizingMaskIntoConstraints = false
        titleLable.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 10).isActive = true
        titleLable.leadingAnchor.constraint(equalTo: iconImageView.leadingAnchor).isActive = true
    }
    
    func setDescriptionLabelConstraints() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.topAnchor.constraint(equalTo: titleLable.bottomAnchor, constant: 10).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: titleLable.leadingAnchor).isActive = true
    }
}
