//
//  CustomTableViewCell.swift
//  ListApp
//
//  Created by Alexander Kononok on 9/20/21.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    var iconImageView = UIImageView()
    var titleLable = UILabel()
    var descriptionLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(iconImageView)
        addSubview(titleLable)
        addSubview(descriptionLabel)
        
        configureImageView()
        configureTitleLabel()
        configureDescriptionLabel()
        
        setImageConstraints()
        setTitleLabelConstraints()
        setDescriptionLabelConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(image: UIImage, title: String, description: String) {
        iconImageView.image = image
        titleLable.text = title
        descriptionLabel.text = description
    }
    
    func configureImageView() {
        iconImageView.layer.cornerRadius = 25
        iconImageView.backgroundColor = .systemGray
        iconImageView.clipsToBounds = true
    }
    
    func configureTitleLabel() {
        titleLable.adjustsFontSizeToFitWidth = true
    }
    
    func configureDescriptionLabel() {
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.textColor = .systemGray
    }
    
    func setImageConstraints() {
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setTitleLabelConstraints() {
        titleLable.translatesAutoresizingMaskIntoConstraints = false
        titleLable.topAnchor.constraint(equalTo: iconImageView.topAnchor).isActive = true
        titleLable.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 10).isActive = true
    }
    
    func setDescriptionLabelConstraints() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.leadingAnchor.constraint(equalTo: titleLable.leadingAnchor).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: -4).isActive = true
    }
}
